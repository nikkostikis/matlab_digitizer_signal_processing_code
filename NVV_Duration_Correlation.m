% Correlation analysis for all NVV and durations / and velocity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Load NVVs
%%%%%%%%%%
folder = 'ProcessedData'
% load ALL NVVs 
load(strcat(folder, '/AllData_PD.mat')); %allData_PD
load(strcat(folder, '/AllData_OH.mat')); %allData_H
% allData_OH = allData_H;
% load(strcat(folder, '/YH/AllData_H.mat')); %allData_H but actually YH
% allData_YH = allData_H;


PD_NVV_cell = allData_PD(:,1); %24x1
OH_NVV_cell = allData_OH(:,1); %20x1
% YH_NVV_cell = allData_YH(:,1); %15x1

PD_NVV = vertcat(PD_NVV_cell{:}); %1068x1
OH_NVV = vertcat(OH_NVV_cell{:}); %804x1
% YH_NVV = vertcat(YH_NVV_cell{:}); %782x1

% load MEAN NVVs
load(strcat(folder, '/PD_Means.mat')); %globM_PD
load(strcat(folder, '/OH_Means.mat')); %globM_H
% globM_OH = globM_OH;
% load(strcat(folder, '/YH/H_Means.mat')); %globM_H but actually YH
% globM_YH = globM_H;

PD_NVV_Mean = globM_PD(:,1); %24x12
OH_NVV_Mean = globM_OH(:,1); %20x12
% YH_NVV_Mean = globM_YH(:,1); %15x12


%Load second factor for correlation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%All values
load(strcat(folder, '/Duration_PD.mat')); %Duration_PD 1x1068
load(strcat(folder, '/Duration_OH.mat')); %Duration_H 1x804
% Duration_OH = Duration_H;
% load(strcat(folder, '/YH/Duration_H.mat')); %Duration_H 1x782 but actually YH
% Duration_YH = Duration_H;

load(strcat(folder, '/Velocity_PD.mat')); %Velocity_PD 1x1068
load(strcat(folder, '/Velocity_OH.mat')); %Velocity_H 1x804
% Velocity_OH = Velocity_H;
% load(strcat(folder, '/YH/Velocity_H.mat')); %Velocity_H 1x782 but actually YH
% Velocity_YH = Velocity_H;


%Calculate Mean values
%%%%%%%%%%%%%%%%%%%%%%

%Find index ranges for each subject OH from All data cell
countVec_OH = ones(20,1);
for i=1:20 
    countVec_OH(i) = length(allData_OH{i, 1});
end

%Find index ranges for each subject YH from All data cell
% countVec_YH = ones(15,1);
% for i=1:15 
%     countVec_YH(i) = length(allData_YH{i, 1});
% end

%Find index ranges for each subject PD from All data cell
countVec_PD = ones(24,1);
for i=1:24 
    countVec_PD(i) = length(allData_PD{i, 1});
end

%Aggregate and average Duration vector OH according to index range
countVec_OH_sum = cumsum(countVec_OH);
durMean_OH = zeros(20,1);
kstart = 1;
for i=1:20
    kend = countVec_OH_sum(i);
    for k=kstart:kend
        durMean_OH(i) = durMean_OH(i) + Duration_OH(k);
    end
    durMean_OH(i) = durMean_OH(i) / countVec_OH(i);
    kstart = countVec_OH_sum(i) + 1;
end

%Aggregate and average Duration vector YH
% countVec_YH_sum = cumsum(countVec_YH);
% durMean_YH = zeros(15,1);
% kstart = 1;
% for i=1:15
%     kend = countVec_YH_sum(i);
%     for k=kstart:kend
%         durMean_YH(i) = durMean_YH(i) + Duration_YH(k);
%     end
%     durMean_YH(i) = durMean_YH(i) / countVec_YH(i);
%     kstart = countVec_YH_sum(i) + 1;
% end

%Aggregate and average Duration vector PD according to index range
countVec_PD_sum = cumsum(countVec_PD);
durMean_PD = zeros(24,1);
kstart = 1;
for i=1:24
    kend = countVec_PD_sum(i);
    for k=kstart:kend
        durMean_PD(i) = durMean_PD(i) + Duration_PD(k);
    end
    durMean_PD(i) = durMean_PD(i) / countVec_PD(i);
    kstart = countVec_PD_sum(i) + 1;
end

%Aggregate and average Velocity vector OH according to index range
countVec_OH_sum = cumsum(countVec_OH);
velMean_OH = zeros(20,1);
kstart = 1;
for i=1:20
    kend = countVec_OH_sum(i);
    for k=kstart:kend
        velMean_OH(i) = velMean_OH(i) + Velocity_OH(k);
    end
    velMean_OH(i) = velMean_OH(i) / countVec_OH(i);
    kstart = countVec_OH_sum(i) + 1;
end

%Aggregate and average Velocity vector YH
% countVec_YH_sum = cumsum(countVec_YH);
% velMean_YH = zeros(15,1);
% kstart = 1;
% for i=1:15
%     kend = countVec_YH_sum(i);
%     for k=kstart:kend
%         velMean_YH(i) = velMean_YH(i) + Velocity_YH(k);
%     end
%     velMean_YH(i) = velMean_YH(i) / countVec_YH(i);
%     kstart = countVec_YH_sum(i) + 1;
% end

%Aggregate and average Velocity vector PD
countVec_PD_sum = cumsum(countVec_PD);
velMean_PD = zeros(24,1);
kstart = 1;
for i=1:24
    kend = countVec_PD_sum(i);
    for k=kstart:kend
        velMean_PD(i) = velMean_PD(i) + Velocity_PD(k);
    end
    velMean_PD(i) = velMean_PD(i) / countVec_PD(i);
    kstart = countVec_PD_sum(i) + 1;
end




%Correlation evaluators
%%%%%%%%%%%%%%%%%%%%%%%

%All values
disp('All values PD_NN Duration_PD');
[r p]=corrcoef(PD_NVV, Duration_PD')
disp('----------------------------');
%No correlation but high p-value (0.12)

disp('All values OH_NVV Duration_OH')
[r p]=corrcoef(OH_NVV, Duration_OH')
disp('----------------------------');
%High correlation (0.47)

disp('All values PD_NVV+OH_NVV Duration_PD+Duration_OH')
[r p]=corrcoef([PD_NVV; OH_NVV], [Duration_PD'; Duration_OH'])
disp('----------------------------');
%Ok correlation (0.24) and 0 p-value

% disp('All values PD_NN Duration_PD')
% [r p]=corrcoef([PD_NVV; OH_NVV; YH_NVV], [Duration_PD'; Duration_OH'; Duration_YH'])
% disp('----------------------------');

disp('All values PD_NVV Velocity_PD')
[r p]=corrcoef(PD_NVV, Velocity_PD')
disp('----------------------------');
%No correlation but high p-value (0.17)

disp('All values OH_NVV Velocity_OH')
[r p]=corrcoef(OH_NVV, Velocity_OH')
disp('----------------------------');
%High correlation (-0.32) 

disp('All values PD_NVV+OH_NVV Velocity_PD+Velocity_OH')
[r p]=corrcoef([PD_NVV; OH_NVV], [Velocity_PD'; Velocity_OH'])
disp('----------------------------');
%Ok correlation (-0.26) and 0 p-value.


% 
% %Mean values
% disp('Mean values PD_NN Duration_PD')
% [r p]=corrcoef(PD_NVV_Mean, durMean_PD)
% disp('----------------------------');
% 
% disp('Mean values PD_NN Duration_PD')
% [r p]=corrcoef(OH_NVV_Mean, durMean_OH)
% disp('----------------------------');
% % 
% disp('Mean values PD_NN Duration_PD')
% [r p]=corrcoef(PD_NVV_Mean, velMean_PD)
% disp('----------------------------');
% 
% disp('Mean values PD_NN Duration_PD')
% [r p]=corrcoef(OH_NVV_Mean, velMean_OH)
% disp('----------------------------');
