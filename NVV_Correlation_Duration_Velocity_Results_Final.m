NVV_Duration_Correlation

folder =

ProcessedData

All values PD_NN Duration_PD

r =

    1.0000    0.1109
    0.1109    1.0000


p =

    1.0000    0.0003
    0.0003    1.0000

----------------------------
All values OH_NVV Duration_OH

r =

    1.0000    0.0678
    0.0678    1.0000


p =

    1.0000    0.0547
    0.0547    1.0000

----------------------------
All values PD_NVV+OH_NVV Duration_PD+Duration_OH

r =

    1.0000    0.1449
    0.1449    1.0000


p =

    1.0000    0.0000
    0.0000    1.0000

----------------------------
All values PD_NVV Velocity_PD

r =

    1.0000   -0.0771
   -0.0771    1.0000


p =

    1.0000    0.0117
    0.0117    1.0000

----------------------------
All values OH_NVV Velocity_OH

r =

    1.0000    0.0191
    0.0191    1.0000


p =

    1.0000    0.5886
    0.5886    1.0000

----------------------------
All values PD_NVV+OH_NVV Velocity_PD+Velocity_OH

r =

    1.0000   -0.0790
   -0.0790    1.0000


p =

    1.0000    0.0006
    0.0006    1.0000

----------------------------