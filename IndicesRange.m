% Calculate range of indices for all files' indexing per person
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [startingInd_PD,endingInd_PD,startingInd_OH,endingInd_OH]=IndicesRange()
%Load NVVs
%%%%%%%%%%
folder = 'ProcessedData'
% load ALL NVVs 
load(strcat(folder, '/AllData_PD.mat')); %allData_PD
load(strcat(folder, '/AllData_H.mat')); %allData_H
allData_OH = allData_H;
% load(strcat(folder, '/YH/AllData_H.mat')); %allData_H but actually YH
% allData_YH = allData_H;

PD_NVV_cell = allData_PD(:,1); %24x1
OH_NVV_cell = allData_OH(:,1); %20x1
% YH_NVV_cell = allData_YH(:,1); %15x1

PD_NVV = vertcat(PD_NVV_cell{:}); %1068x1
OH_NVV = vertcat(OH_NVV_cell{:}); %804x1
% YH_NVV = vertcat(YH_NVV_cell{:}); %782x1

%Find index ranges (start-end) for each subject OH from All data cell
countVec_OH = cellfun('length', allData_OH(:,1));
startingInd_OH = ones(length(countVec_OH),1);
endingInd_OH = cumsum(countVec_OH);
temp = [0; endingInd_OH];
startingInd_OH = startingInd_OH + temp(1:end-1);

%Find index ranges (start-end) for each subject PD from All data cell
countVec_PD = cellfun('length', allData_PD(:,1));
startingInd_PD = ones(length(countVec_PD),1);
endingInd_PD = cumsum(countVec_PD);
temp = [0; endingInd_PD];
startingInd_PD = startingInd_PD + temp(1:end-1);

