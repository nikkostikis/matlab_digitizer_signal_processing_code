% Called by runner_Testr

function IntraNormality()
FilesReference

fprintf('Intra Normality for H population \n\n');
pcount=max(size(allData_H));
pJB=ones(pcount);
pLillie=ones(pcount);
for i=1:pcount
    label=strcat('All Data for subject  ',num2str(i));
    [hJB pJB(i) hLillie pLillie(i)]=normality(allData_H{i,1},label);
end


fprintf('Intra Normality for PD population \n\n');
pcount=max(size(allData_PD));
pJB=ones(pcount);
pLillie=ones(pcount);
for i=1:pcount
    label=strcat('All Data for subject  ',num2str(i));
    [hJB pJB(i) hLillie pLillie(i)]=normality(allData_PD{i,1},label);
end


%pause;