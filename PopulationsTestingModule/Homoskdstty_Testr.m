% FilesReference
% MEAN values:
% ALL -> globM_XX(:,1)
% LH -> glob2_XX(:,4)
% HH -> glob2_XX(:,1)
% HLH -> glob3_XX(:,1)
% LD -> glob6_XX
% HD -> glob4_XX
% Left -> globM_XX(:,2)
% Right -> globM_XX(:,3)

fprintf('\nStart%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n\n');
fprintf('\nHomoscedasticity for MEANS\n');
homosked(globM_H(:,1), globM_PD(:,1), 'ALL');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob2_H(:,4), glob2_PD(:,4), 'LH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob2_H(:,1), glob2_PD(:,1), 'HH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob3_H(:,1), glob3_PD(:,1), 'HLH');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob6_H, glob6_PD, 'LD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob4_H, glob4_PD, 'HD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

homosked(globM_H(:,2), globM_H(:,3), 'Left vs Right Healthy');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(globM_PD(:,2), globM_PD(:,3), 'Left vs Right PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

homosked(glob2_H(:,4), glob2_H(:,1), 'LH vs HH Healthy');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');
homosked(glob2_PD(:,4), glob2_PD(:,1), 'LH vs HH PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');




% 
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob2_H(:,1), glob2_H(:,4), 'wh vs bh intra N');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob2_H(:,1), globM_H(:,1), 'wh vs allh intra N');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob4_H, globM_H(:,1), 'wd vs allh intra N');
% % homosked(glob4_H, globM_H(:,5), 'wd vs f intra N');
% % homosked(glob4_H, globM_H(:,4), 'wd vs e intra N');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob4_H, glob6_H, 'wd vs bd intra N');
% 
% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob2_PD(:,1), glob2_PD(:,4), 'wh vs bh intra PD');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob2_PD(:,1), globM_PD(:,1), 'wh vs allh intra PD');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob4_PD, globM_PD(:,1), 'wd vs allh intra PD');
% % homosked(glob4_PD, globM_PD(:,5), 'wd vs f intra PD');
% % homosked(glob4_PD, globM_PD(:,4), 'wd vs e intra PD');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob4_PD, glob6_PD, 'wd vs bd intra PD');
% 
% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globM_H(:,1), globM_PD(:,1), 'allh inter');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob2_H(:,1), glob2_PD(:,1), 'wh inter');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob4_H, glob4_PD, 'wd inter');
% fprintf('\nNew test follows==============================\n\n');
% homosked(glob3_H(:,1), glob3_PD(:,1), 'wh-bh inter');
% % homosked(globM_H(:,6), globM_PD(:,6), 'le inter');
% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% 
% 
% fprintf('\nHomoscedasticity for ALL\n');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globH_H{1}, globH_H{4}, 'wh vs bh intra N');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globH_H{1}, globA_H{9}, 'wh vs allh intra N');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globHD_H, globA_H{9}, 'wd vs allh intra N');
% % homosked(globHD_H, globA_H{4}, 'wd vs f intra N');
% % homosked(globHD_H, globA_H{3}, 'wd vs e intra N');
% % homosked(globA_H{5}, globA_H{11}, 'le vs allothers intra N');
% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globH_PD{1}, globH_PD{4}, 'wh vs bh intra PD');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globH_PD{1}, globA_PD{9}, 'wh vs allh intra PD');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globHD_PD, globA_PD{9}, 'wd vs allh intra PD');
% % homosked(globHD_PD, globA_PD{4}, 'wd vs f intra PD');
% % homosked(globHD_PD, globA_PD{3}, 'wd vs e intra PD');
% % homosked(globA_PD{5}, globA_PD{11}, 'le vs allothers intra PD');
% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globA_H{9}, globA_PD{9}, 'allh inter');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globH_H{1}, globH_PD{1}, 'wh inter');
% fprintf('\nNew test follows==============================\n\n');
% homosked(globHD_H, globHD_PD, 'wd inter');
% % homosked(abs(globH_H{1}-globH_H{4}),abs(globH_PD{1}-globH_PD{4}),'wh-bh inter');
% % homosked(globA_H{5}, globA_PD{5}, 'le inter');
