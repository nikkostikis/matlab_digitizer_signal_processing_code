FilesReference
% MEAN values:
% ALL -> globM_XX(:,1)
% LH -> glob2_XX(:,4)
% HH -> glob2_XX(:,1)
% HLH -> glob3_XX(:,1)
% LD -> glob6_XX
% HD -> glob4_XX
% Left -> globM_XX(:,2)
% Right -> globM_XX(:,3)

MeanH=zeros(8,1);
StdH=zeros(8,1);

MeanPD=zeros(8,1);
StdPD=zeros(8,1);

fprintf('\nStart Normality Tester%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\n HEALTHY SUBJECTS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\nNormality for MEANS ALL Healthy\n\n');
[hJBallH, hLillieallH]=normality(globM_H(:,1),'ALL H');
[MeanH(1,1), StdH(1,1)]=meanStd(globM_H(:,1),'ALL H');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS LH Healthy\n\n');
[hJBlhH, hLillielhH]=normality(glob2_H(:,4),'LH H');
[MeanH(2,1), StdH(2,1)]=meanStd(glob2_H(:,4),'LH H');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HH Healthy\n\n');
[hJBhhH, hLilliehhH]=normality(glob2_H(:,1),'HH H');
[MeanH(3,1), StdH(3,1)]=meanStd(glob2_H(:,1),'HH H');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HLH Healthy\n\n');
[hJBwhlhH, hLilliewhlhH]=normality(glob3_H(:,1),'HLH H');
[MeanH(4,1), StdH(4,1)]=meanStd(glob3_H(:,1),'HLH H');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS LD Healthy\n\n');
[hJBldH, hLillieldH]=normality(glob6_H,'LD H');
[MeanH(5,1), StdH(5,1)]=meanStd(glob6_H,'LD H');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HD Healthy\n\n');
[hJBhdH, hLilliehdH]=normality(glob4_H,'HD H');
[MeanH(6,1), StdH(6,1)]=meanStd(glob4_H,'HD H');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS Left Healthy\n\n');
[hJBLeftH, hLillieLeftH]=normality(globM_H(:,2),'Left H');
[MeanH(7,1), StdH(7,1)]=meanStd(globM_H(:,2),'Left H');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS Right Healthy\n\n');
[hJBRightH, hLillieRightH]=normality(globM_H(:,3),'Right H');
[MeanH(8,1), StdH(8,1)]=meanStd(globM_H(:,3),'Right H');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\n PD SUBJECTS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf('\nNormality for MEANS ALL PD\n\n');
[hJBallPD, hLillieallPD]=normality(globM_PD(:,1),'ALL PD');
[MeanPD(1,1), StdPD(1,1)]=meanStd(globM_PD(:,1),'ALL PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS LH PD\n\n');
[hJBlhPD, hLillielhPD]=normality(glob2_PD(:,4),'LH PD');
[MeanPD(2,1), StdPD(2,1)]=meanStd(glob2_PD(:,4),'LH PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HH PD\n\n');
[hJBhhPD, hLilliehhPD]=normality(glob2_PD(:,1),'HH PD');
[MeanPD(3,1), StdPD(3,1)]=meanStd(glob2_PD(:,1),'HH PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HLH PD\n\n');
[hJBwhlhPD, hLilliewhlhPD]=normality(glob3_PD(:,1),'HLH PD');
[MeanPD(4,1), StdPD(4,1)]=meanStd(glob3_PD(:,1),'HLH PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS LD PD\n\n');
[hJBldPD, hLillieldPD]=normality(glob6_PD,'LD PD');
[MeanPD(5,1), StdPD(5,1)]=meanStd(glob6_PD,'LD PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS HD PD\n\n');
[hJBhdPD, hLilliehdPD]=normality(glob4_PD,'HD PD');
[MeanPD(6,1), StdPD(6,1)]=meanStd(glob4_PD,'HD PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS Left PD\n\n');
[hJBLeftPD, hLillieLeftPD]=normality(globM_PD(:,2),'Left PD');
[MeanPD(7,1), StdPD(7,1)]=meanStd(globM_PD(:,2),'Left PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nNormality for MEANS Right PD\n\n');
[hJBRightPD, hLillieRightPD]=normality(globM_PD(:,3),'Right PD');
[MeanPD(8,1), StdPD(8,1)]=meanStd(globM_PD(:,3),'Right PD');
fprintf('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%\n\n');

fprintf('\nEnd Normality Tester%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n\n');






% fprintf('\nStart%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% fprintf('\nNormality for MEANS\n\n');
% [hJBallN hLillieallN]=normality(globM_H(:,1),'allh N');
% [hJBwhN hLilliewhN]=normality(glob2_H(:,1),'wh N');
% [hJBbhN hLilliebhN]=normality(glob2_H(:,4),'bh N');
% [hJBwdN hLilliewdN]=normality(glob4_H,'wd N');
% [hJBfN hLilliefN]=normality(globM_H(:,5),'F N');
% [hJBeN hLillieeN]=normality(globM_H(:,4),'E N');
% [hJBfN hLilliefN]=normality(globM_H(:,2),'L N');
% [hJBeN hLillieeN]=normality(globM_H(:,3),'R N');
% [hJBwhbhN hLilliewhbhN]=normality(glob3_H(:,1),'wh-bh N');
% 
% 
% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% [hJBallPD hLillieallPD]=normality(globM_PD(:,1),'allh PD');
% [hJBwhPD hLilliewhPD]=normality(glob2_PD(:,1),'wh PD');
% [hJBbhPD hLilliebhPD]=normality(glob2_PD(:,4),'bh PD');
% [hJBwdPD hLilliewdPD]=normality(glob4_PD,'wd PD');
% [hJBfPD hLilliefPD]=normality(globM_PD(:,5),'f PD');
% [hJBePD hLillieePD]=normality(globM_PD(:,4),'e PD');
% [hJBlPD hLillilPD]=normality(globM_PD(:,2),'L PD');
% [hJBrPD hLillierPD]=normality(globM_PD(:,3),'R PD');
% [hJBwhbhPD hLilliewhbhPD]=normality(glob3_PD(:,1),'wh-bh PD');

% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% fprintf('\nNormality for ALL\n\n');
% [hJBallN hLillieallN]=normality(globA_H{9},'allh N');
% [hJBwhN hLilliewhN]=normality(globH_H{1},'wh N');
% [hJBbhN hLilliebhN]=normality(globH_H{4},'bh N');
% [hJBwdN hLilliewdN]=normality(globHD_H,'wd N');
% [hJBfN hLilliefN]=normality(globA_H{4},'f N');
% [hJBeN hLillieeN]=normality(globA_H{3},'e N');
% [hJBlN hLillielN]=normality(globA_H{1},'L N');
% [hJBrN hLillierN]=normality(globA_H{2},'R N');
% 
% fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
% [hJBallPD hLillieallPD]=normality(globA_PD{9},'allh PD');
% [hJBwhPD hLilliewhPD]=normality(globH_PD{1},'wh PD');
% [hJBbhPD hLilliebhPD]=normality(globH_PD{4},'bh PD');
% [hJBwdPD hLilliewdPD]=normality(globHD_PD,'wd PD');
% [hJBfPD hLilliefPD]=normality(globA_PD{4},'f PD');
% [hJBePD hLillieePD]=normality(globA_PD{3},'e PD');
% [hJBlPD hLillielPD]=normality(globA_PD{1},'L PD');
% [hJBrPD hLillierPD]=normality(globA_PD{2},'R PD');

