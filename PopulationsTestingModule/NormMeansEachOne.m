FilesReference
fprintf('Normality and Means testing for each H subject for left vs right hand \n\n');

for i=1:length(allData_H)
    i
    [hJBLN hLillieLN]=normality(allData_H{i,2},'L H');
    [hJBRN hLillieRN]=normality(allData_H{i,3},'R H');
    
    tstt(allData_H{i,2},allData_H{i,3}, 'left vs right for each H','unequal');
    tstt(allData_H{i,2},allData_H{i,3}, 'left vs right for each H','equal');
    mw(allData_H{i,2},allData_H{i,3}, 'left vs right for each H');
    fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
end

fprintf('Normality and Means testing for each PD subject for left vs right hand \n\n');

for i=1:length(allData_PD)
    i
    [hJBLPD hLillieLPD]=normality(allData_PD{i,2},'L PD');
    [hJBRPD hLillieRPD]=normality(allData_PD{i,3},'R PD');
    
    tstt(allData_PD{i,2},allData_PD{i,3}, 'left vs right for each PD','unequal');
    tstt(allData_PD{i,2},allData_PD{i,3}, 'left vs right for each PD','equal');
    mw(allData_PD{i,2},allData_PD{i,3}, 'left vs right for each PD');
    fprintf('\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
