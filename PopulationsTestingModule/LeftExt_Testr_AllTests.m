load 'ProcessedData/SindexN_MEANS.mat'  %matrix globM_N -->mean data 12 columns 1.ALL 2.L 3.R 4.E 5.F 6.LE 7.LF 8.RE 9.RF 10.EF 11.R+LF 12.AllWithoutWD
load 'ProcessedData/SindexPD_MEANS.mat' %matrix globM_PD -->mean data 12 columns


fprintf('\nNormality Testing\n\n');

fprintf('\n\nNormality Testing for LeftExtension Normal @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
[hJBlfN hLillielfN]=normality(globM_N(:,6),'left extension N');

fprintf('\n\nNormality Testing for Right+LeftFlexion Normal @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
[hJBrlfN hLillierlfN]=normality(globM_N(:,11),'right+left flexion N');

fprintf('\n\nNormality Testing for LeftExtension PD @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
[hJBlfPD hLillielfPD]=normality(globM_PD(:,6),'left extension PD');

fprintf('\n\nNormality Testing for Right+LeftFlexion PD @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
[hJBrlfPD hLillierlfPD]=normality(globM_PD(:,11),'right+left flexion PD');




fprintf('\nMean & Std Dev values\n\n');
MeanValues=zeros(4,1);
Stds=zeros(4,1);

fprintf('\n\nMean value & StdDev for LeftExtension N @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
[MeanValues(1,1) Stds(1,1)]=meanStd(globM_N(:,6),'LeftExtension N');

fprintf('\n\nMean value & StdDev for Right+LeftFlexion N @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
[MeanValues(2,1) Stds(2,1)]=meanStd(globM_N(:,11),'Right+LeftFlexion N');

fprintf('\n\nMean value & StdDev for LeftExtension PD @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
[MeanValues(3,1) Stds(3,1)]=meanStd(globM_PD(:,6),'LeftExtension PD');

fprintf('\n\nMean value & StdDev for Right+LeftFlexion PD @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
[MeanValues(4,1) Stds(4,1)]=meanStd(globM_PD(:,11),'Rigt+LeftFlexion PD');




fprintf('\nHomoscedasticity\n\n');
fprintf('\n\nHomoscedasticity for LeftExtension vs Right+LeftFlexion intra N @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
homosked(globM_N(:,6), globM_N(:,11), 'le vs allothers intra N');

fprintf('\n\nHomoscedasticity for LeftExtension vs Right+LeftFlexion intra PD @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
homosked(globM_PD(:,6), globM_PD(:,11), 'le vs allothers intra Pk');

fprintf('\n\nHomoscedasticity for LeftExtension inter @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
homosked(globM_N(:,6), globM_PD(:,6), 'le inter');




fprintf('\nMeans Equality\n\n');
fprintf('\n\nMeans Equality for LeftExtension vs Right+LeftFlexion intra N @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
tstt(globM_N(:,6), globM_N(:,11), 'LE vs R+LF intra N','unequal');
tstt(globM_N(:,6), globM_N(:,11), 'LE vs R+LF intra N','equal');
mw(globM_N(:,6), globM_N(:,11), 'LE vs R+LF intra N');

fprintf('\n\nMeans Equality for LeftExtension vs Right+LeftFlexion intra PD @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
tstt(globM_PD(:,6), globM_PD(:,11), 'LE vs R+LF intra PD','unequal');
tstt(globM_PD(:,6), globM_PD(:,11), 'LE vs R+LF intra PD','equal');
mw(globM_PD(:,6), globM_PD(:,11), 'LE vs R+LF intra PD');


fprintf('\n\nMeans Equality for LeftExtension inter @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n');
tstt(globM_N(:,6), globM_PD(:,6), 'LE inter','unequal');
tstt(globM_N(:,6), globM_PD(:,6), 'LE inter','equal');
mw(globM_N(:,6), globM_PD(:,6), 'LE inter');
