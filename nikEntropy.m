function entp = nikEntropy(vec, numOfDigits)
% if the vec consists of real values, the numOfDigits acts like a binning 
% algorithm, rounding the values and reducing the unique Alphabet 'symbols'

% Establish size of data
n = numel(vec);

P = zeros(n,1);

if nargin > 1
% Perform binning simulation by rounding to required number of digits
% and retrieve uniqe values
vec = round(vec*10^numOfDigits) / 10^numOfDigits;
end


% Assemble observed alphabet
Alphabet = unique(vec);

% Housekeeping
Frequency = zeros(size(Alphabet));

% Calculate sample frequencies
for point = 1:numel(Alphabet)
    Frequency(point) = sum(vec == Alphabet(point));
end

% Calculate sample class probabilities
P = Frequency / sum(Frequency);

% Calculate entropy in bits
% Note: floating point underflow is never an issue since we are
%   dealing only with the observed alphabet
entp = -sum(P .* log2(P));
