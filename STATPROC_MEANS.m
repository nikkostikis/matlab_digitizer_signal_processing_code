%STATISTICAL PROCESSING

%read data from ProcessedData directory and run statistical tests
clear all;


load 'ProcessedData/OH_Means.mat'
load 'ProcessedData/PD_Means.mat'
%there is 1 matrix, globM_OH (Healthy) and globM_PD (PD)
%each contains 1 row per subject, 11 columns in the order
%ALL, L R E F LE LF RE RF EF R+LF

load 'ProcessedData/WhichWorstHand_OH.mat'
load 'ProcessedData/WhichWorstHand_PD.mat'
%there is 1 matrix Hand_OH and Hand_PD, each contains
%1 row per subject and 2 columns, 1st numOfSubject (redundant)
%2nd 1 for left, -1 for right worst (best) hand

load 'ProcessedData/WhichWorstDirection_OH.mat'
load 'ProcessedData/WhichWorstDirection_PD.mat'
%there is 1 matrix Direction_OH and Direction_PD, each contains
%1 row per subject and 2 columns, 1st numOfSubject (redundant)
%2nd 2 for extension, -2 for flexion as worst direction of movement

load 'ProcessedData/WhichBestHandandDirection_OH.mat'
load 'ProcessedData/WhichBestHandandDirection_PD.mat'
%there is 1 matrix BDirection_OH and BDirection_PD, each contains
%1 row per subject and 3 columns, 1st numOfSubject (redundant)
%2nd 2 for extension, -2 for flexion as best direction of movement
%3rd 1 for left, -1 for right as best hand


%Get current folder to save figure later on
tempF=pwd; 
idxF=find(tempF=='/');
idxF=max(idxF);
folder=tempF(idxF+1:length(tempF));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




fprintf('\nSTATPROC_MEANS is running---------------------------\n');

pcount_OH=size(globM_OH);
pcount_PD=size(globM_PD);
for i=1:pcount_OH
    if  (Hand_OH(i,2)==1) %Left hand worst
        glob2_OH(i,:)=[globM_OH(i,2),globM_OH(i,6),globM_OH(i,7),globM_OH(i,3)];
        glob3_OH(i,:)=[globM_OH(i,2)-globM_OH(i,3),...
                        globM_OH(i,6)-globM_OH(i,8),...
                            globM_OH(i,7)-globM_OH(i,9)];
        if (Direction_OH(i,2)==2) %Keep Left Extension
            glob4_OH(i,:)=globM_OH(i,6);
            glob5_OH(i,:)=globM_OH(i,6)-globM_OH(i,7); %wrong because I have (worst hand&best direction), which doesn't work
        else %Keep Left Flexion
            glob4_OH(i,:)=globM_OH(i,7);
            glob5_OH(i,:)=globM_OH(i,7)-globM_OH(i,6);
        end
    else %Right hand worst
        glob2_OH(i,:)=[globM_OH(i,3),globM_OH(i,8),globM_OH(i,9),globM_OH(i,2)];
        glob3_OH(i,:)=[globM_OH(i,3)-globM_OH(i,2),...
                        globM_OH(i,8)-globM_OH(i,6),...
                            globM_OH(i,9)-globM_OH(i,7)];
        if (Direction_OH(i,2)==2) %Keep Right Extension
            glob4_OH(i,:)=globM_OH(i,8);
            glob5_OH(i,:)=globM_OH(i,8)-globM_OH(i,9);
        else %Keep Right Flexion
            glob4_OH(i,:)=globM_OH(i,9);
            glob5_OH(i,:)=globM_OH(i,9)-globM_OH(i,8);
        end
    end
    
    %Best Direction segment
    if (BDirection_OH(i,2)==2) %best direction Ext
        if (BDirection_OH(i,3)==1) %best hand L
            glob6_OH(i,:)=globM_OH(i,6); %LE
        else %best hand R
            glob6_OH(i,:)=globM_OH(i,8); %RE
        end
    else %best direction Flex
        if (BDirection_OH(i,3)==1) %best hand L
            glob6_OH(i,:)=globM_OH(i,7); %LF
        else %best hand R
            glob6_OH(i,:)=globM_OH(i,9); %RF
        end
    end
    %End of best direction segment
        
        
end

for i=1:pcount_PD
    if  (Hand_PD(i,2)==1) %Left Hand worst
        glob2_PD(i,:)=[globM_PD(i,2),globM_PD(i,6),globM_PD(i,7),globM_PD(i,3)];
        glob3_PD(i,:)=[globM_PD(i,2)-globM_PD(i,3),...
                        globM_PD(i,6)-globM_PD(i,8),...
                            globM_PD(i,7)-globM_PD(i,9)];
        if (Direction_PD(i,2)==2) %Keep Left Extension
            glob4_PD(i,:)=globM_PD(i,6);
            glob5_PD(i,:)=globM_PD(i,6)-globM_PD(i,7);
        else %Keep Left Flexion
            glob4_PD(i,:)=globM_PD(i,7);
            glob5_PD(i,:)=globM_PD(i,7)-globM_PD(i,6);
        end
    else %Right Hand worst
        glob2_PD(i,:)=[globM_PD(i,3),globM_PD(i,8),globM_PD(i,9),globM_PD(i,2)];
        glob3_PD(i,:)=[globM_PD(i,3)-globM_PD(i,2),...
                        globM_PD(i,8)-globM_PD(i,6),...
                            globM_PD(i,9)-globM_PD(i,7)];
        if (Direction_PD(i,2)==2) %Keep Right Extension
            glob4_PD(i,:)=globM_PD(i,8);
            glob5_PD(i,:)=globM_PD(i,8)-globM_PD(i,9);
        else %Keep Right Flexion
            glob4_PD(i,:)=globM_PD(i,9);
            glob5_PD(i,:)=globM_PD(i,9)-globM_PD(i,8);
        end
    end

    %Best Direction segment
    if (BDirection_PD(i,2)==2) %best direction Ext
        if (BDirection_PD(i,3)==1) %best hand L
            glob6_PD(i,:)=globM_PD(i,6); %LE
        else %best hand R
            glob6_PD(i,:)=globM_PD(i,8); %RE
        end
    else %best direction Flex
        if (BDirection_PD(i,3)==1) %best hand L
            glob6_PD(i,:)=globM_PD(i,7); %LF
        else %best hand R
            glob6_PD(i,:)=globM_PD(i,9); %RF
        end
    end
    %End of best direction segment
    
end
%glob2_OH contains 4 columns, L,LE,LF,R or R,RE,RF,L dependent on the the Hand_OH
%matrix, same goes for glob2_PD

%glob3_OH contains 3 columns L-R, LE-RE, LF-RF or R-L, RE-LE, RF-LF
%dependent on the Hand_OH matrix, same goes for glob3_PD

save('ProcessedData/WorstHand_OH_MEANS.mat','glob2_OH');%Mean values, Worst Hand x4
save('ProcessedData/WorstHand_PD_MEANS.mat','glob2_PD');
save('ProcessedData/WorstHandDirection_OH_MEANS.mat','glob4_OH');%Mean values, WorstHand Direction x1
save('ProcessedData/WorstHandDirection_PD_MEANS.mat','glob4_PD');
save('ProcessedData/DiafWorstBestHand_OH_MEANS.mat','glob3_OH');%Mean values, Worst-Best x3
save('ProcessedData/DiafWorstBestHand_PD_MEANS.mat','glob3_PD');
save('ProcessedData/DiafWorstBestHandDirection_OH_MEANS.mat','glob5_OH');%Mean values, WorstHand Direction-BestHand Direction x1
save('ProcessedData/DiafWorstBestHandDirection_PD_MEANS.mat','glob5_PD');
save('ProcessedData/BestDirection_OH_MEANS.mat', 'glob6_OH'); %mean data Best Direction x1
save('ProcessedData/BestDirection_PD_MEANS.mat', 'glob6_PD'); 


%Separation groups
globSep_OH=[glob2_OH(), globM_OH(:,1:9), glob4_OH(), glob3_OH(:,1), glob6_OH()];
globSep_PD=[glob2_PD(), globM_PD(:,1:9), glob4_PD(), glob3_PD(:,1), glob6_PD()];
Labels={'Worst_OHand', 'Worst_OHand_Extension', 'Worst_OHand_Flexion', 'Best_OHand',...
    'ALL_OHands', 'Left_OHand', 'Right_OHand', 'Extension', 'Flexion',...
    'Left_Extension', 'Left_Flex', 'Right_Extension', 'Right_Flex',...
    'Worst_Direction', 'Worst_OHand-Best_OHand', 'Best_Direction'};

for i=1:16
    PDnik=deNaNize(globSep_PD(:,i));    
    Hnik=deNaNize(globSep_OH(:,i));
    roc_nik(PDnik, Hnik, Labels{i});
end

% for i=1:16
%     Nnik=deNaNize(globSep_OH(:,i));
%     PDnik=deNaNize(globSep_PD(:,i));
%     maxn=max(Nnik);
%     minn=min(Nnik);
%     maxpk=max(PDnik);
%     minpk=min(PDnik);
%     maxf=max(maxn,maxpk);
%     minf=min(minn,minpk);
%     step=(maxf-minf)/1000;
%     edges(i,:)=[minf:step:maxf,inf];
%     nn=histc(Nnik,edges(i,:));
%     nn=nn/sum(nn)*100;
%     npk=histc(PDnik,edges(i,:));
%     npk=npk/sum(npk)*100;
%     
%     %xxx find best separator value
%     [temp len]=size(edges);
%     for j=1:len
%         tp=sum(npk(j:len)); %true positive
%         if j==1
%             fn=0;
%         else
%             fn=sum(npk(1:(j-1)));   %false negative
%         end
%         fp=sum(nn(j:len));  %false positive
%         if j==1
%             tn=0;
%         else            
%             tn=sum(nn(1:(j-1)));    %true negative
%         end
%         %lamda value must be maximum
% %         lamda=(tp*tn)/((fp*fn)+1);
%         lamda=tn;
% %         lamda=tp;
% 
%         edgeslamda(i,j)=lamda;
%     end
%     [temp2 ind]=max(edgeslamda(i,:));
%     
%     separator(i)=edges(i,ind);
%     sucfactor(i)=edgeslamda(i,ind)
% 
%    
%     finaltp(i)=sum(npk(ind:length(npk)));
%     finaltn(i)=sum(nn(1:ind-1));
%     finalfp(i)=sum(nn(ind:length(nn)));
%     finalfn(i)=sum(npk(1:ind-1));
%     
%     fprintf('Label=%s\n', Labels{i});
%     fprintf('TPOS=%f, TNEG=%f, FP=%f, FN=%f, SEPARATOR=%f\n',finaltp(i),finaltn(i),finalfp(i),finalfn(i),separator(i)); 
%    
% %%%%%%% Uncomment this to build Distribution Figure%%%%%%%%%%%%%
% %     figure(3);
% %     Nedges=length(edges)-1;
% %     hold off;
% %     xlabel('Mean Score (worst hand)');
% %     ylabel('Percentage of population');
% %     hold on;
% %     bar(edges(i,1:Nedges),[nn(1:Nedges) npk(1:Nedges)],'stack')
% %     legend('N','PD');
% %     colormap bone;
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
%     
%     h=figure(4);
%     hold off;
%     plot(edges(i,:), npk);
%     title(Labels{i});
%     xlabel('Mean Values of index');
%     ylabel('Percentage of population');
%     hold on;
%     plot(edges(i,:),nn,'r');
%     legend('PD','N');
%     percent=[0 10 20 30 40 50 60 70 80];
%     value=ones(9)*separator(i);
%     plot(value,percent,'g');
%     annot1=strcat('<- point of distinction: ',num2str(separator(i)));
%     text(separator(i),40,annot1,'HorizontalAlignment','left');
%     annot2=strcat('success factor: ',num2str(sucfactor(i)));
%     text((separator(i)+0.0018),36,annot2,'HorizontalAlignment','left');
%     text((separator(i)+0.0018),32, strcat('TPOS=',num2str(finaltp(i)),' TNEG=',num2str(finaltn(i))), 'HorizontalAlignment','left');
%     text((separator(i)+0.0018),28, strcat('FP=',num2str(finalfp(i)),' FN=',num2str(finalfn(i))), 'HorizontalAlignment','left');
%     text((separator(i)+0.0018),24, strcat('SEPARATOR=',num2str(separator(i))), 'HorizontalAlignment','left');
%         
%     tag=Labels{i};
%     mkdir(strcat('../Separations/',folder));
%     saveas(h,strcat('../Separations/',folder,'/S_',tag),'jpg');
%  
% %     pause;
% 
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---THE END---%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
