% Choose the metric through comments in the basic_new
% Choose the axis/axes here
% Create folders ProcessedData, ROCs and ROCs_Figs
% Run
clear all;
clc;
global thetaSum;
thetaSum=0;
% index = 'NVV';
% index = 'MV';
% index = 'SDV';
% index = 'ETPv';
% index = 'ETPy';
% index = 'ETPy4';
% index = 'ETPyRot';
% index = 'ETPyRot4';
% index = 'Theta'; % not a good feature

% sampling = 'high'; %i.e. 133Hz
sampling = 'low'; %i.e. 60Hz

% Prepare txt file for all PD
% mfile=strcat('Basic_Results_PD','_',index,'.txt');
% mfid=fopen(mfile,'w+');
% fprintf(mfid,'Fname\tResolution\tDuration\tQindex\tMeanVel\n');
% fclose(mfid);
% 
% mfile=strcat('Percentage_Same_Time_PD.txt');
% mfid=fopen(mfile,'w+');
% fprintf(mfid,'Fname\tPercentageSameTime\n');
% fclose(mfid);

batch_new('PD','ALL',index,sampling)
batch_new('PD','MEANS',index,sampling)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prepare txt file for all H
% mfile=strcat('Basic_Results_H','_',index,'.txt');
% mfid=fopen(mfile,'w+');
% fprintf(mfid,'Fname\tResolution\tDuration\tQindex\tMeanVel\n');
% fclose(mfid);
% 
% mfile=strcat('Percentage_Same_Time_H.txt');
% mfid=fopen(mfile,'w+');
% fprintf(mfid,'Fname\tPercentageSameTime\n');
% fclose(mfid);
batch_new('OH','ALL',index,sampling)
batch_new('OH','MEANS',index,sampling)

% It creates differences (Worst-Best) and basic comparisons
% STATPROC
STATPROC_MEANS